module bitbucket.org/anemone-es/oksvg

go 1.17

require (
	bitbucket.org/anemone-es/gofpdf v0.0.0-20211020165003-88588559cc9d
	github.com/srwiley/rasterx v0.0.0-20210519020934-456a8d69b780
	golang.org/x/image v0.0.0-20210628002857-a66eb6448b8d
	golang.org/x/net v0.0.0-20211011170408-caeb26a5c8c0
	golang.org/x/text v0.3.6 // indirect
)
